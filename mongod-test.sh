#!/bin/bash
  
if [[ -z "$1" || -z "$2" || -z "$3" ]]
  then
    echo "Please specify the test name"
    echo "\"replicamembers\" To check the members in replicaset"
    echo "\"checkprimary\" To check node is primary or secondary"
    echo "\"health\" For check the state of the shards"
    echo "\"checkwrite\" To check is node is readonly"
    echo "\"conn\" Check the connection status"
    echo "Example: ./mongod-test.sh replicamembers <ip> <port>"
    echo $1 $2 $3
    exit 1
fi
test_case=$1;
bind_ip=$2;
bind_port=$3;
mongo --host $bind_ip --port $bind_port admin --quiet --eval "
var selections='"''$test_case''"';
try{
if(selections == 'replicamembers'){
	members=db.isMaster().hosts.length;
    if (members > 2 && members < 5){
		print('Current members are', members, 'and looking good');
	}else{
		print('There is something messy, please check the replicaset');
	}
}else if (selections == 'checkprimary'){
	isPrimary=db.isMaster().ismaster;
    if (isPrimary == true){
		print('Node is Primary', isPrimary);
	}else{
		print('Node is Secondary');
	}
}else if (selections == 'health'){
	state=db.isMaster().ok;
    if (state == 1){
		print('Node is Healthy');
	}else{
		print('Node is not Healthy');
	}
}else if (selections == 'checkwrite'){
	isreadOnly=db.isMaster().readOnly;
    if (isreadOnly == false){
		print('Node is not in READ ONLY');
	}else{
		print('This node is in READ ONLY');
	}
}else if (selections == 'conn'){
	connections=db.serverStatus().connections;
    if (connections.available < 1000){
    	print('DB Connections getting exhausted');
	}else{
		print('DB Connections are available', 'Current:',connections.current, 'Available:',connections.available, 'TotalCreated:',connections.totalCreated, 'Active:',connections.active);
	}	
}else{
	print('Enter valid tests');
}
}catch(err){
	print('DB Exception:', err.message);
}"