#!/bin/bash

if [[ -z "$1" || -z "$2" || -z "$3" ]]
  then
    echo "Please specify the test name"
    echo "\"balancer\" for testing the balancer status"
    echo "\"connections\" for testing the number connections and status"
    echo "\"shard\" for check the state of the shards"
    echo "Example: ./mongos-test.sh balancer <ip> <port>"
    echo $1 $2 $3
    exit 1
fi




test_func(){
 echo "testing function"
}

balancer(){
	#echo $bind_ip $bind_port
	mongo --host $bind_ip --port $bind_port admin --quiet --eval "
	var balancer;
	balancer=db.adminCommand( { balancerStatus: 1 } ).ok
	if(balancer = 1){
		print('Balancer is enabled');
	}else{
		print('Balancer is diabled');
	}"
}


connections(){
	mongo --host $bind_ip --port $bind_port admin --quiet --eval "
	var data;
	data=db.serverStatus().connections;
	print('CURRENT:', data.current, 'AVAILABLE:', data.available, 'TOTALCREATED:', data.totalCreated);
	if (data.available < 1000){
	     print('DB Connections getting exhausted');
	}else{
	     print('DB Connections are available');
	}"
}

listShard(){
       	mongo --host $bind_ip --port $bind_port admin --quiet --eval "
	var length;
	var state;
	var shard;
	length=db.adminCommand({ listShards: 1 }).shards.length
	print('TotalShards:',length)
	for (var i = 0; i < length; i++) {
	  shard=db.adminCommand({ listShards: 1 }).shards[i].host;  
	  state=db.adminCommand({ listShards: 1 }).shards[i].state;
	  if (state = 1){
	  	print('HOSTS:',shard,'STATE:',state,'Sharding is running');
	  }else{
	    print('HOSTS:',shard,'STATE:',state,'Shard is down', shard)
	  }
	}" 
}

test=$1
bind_ip=$2
bind_port=$3

case $test in

  balancer)
    balancer $bind_ip $bind_port
    ;;

  connections)
    connections $bind_ip $bind_port
    ;;

  shard)
    listShard $bind_ip $bind_port
    ;; 

  test)
    test_func
    ;;

  *)
    echo -n "unknown"
    ;;
esac
